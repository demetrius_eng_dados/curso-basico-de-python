from Criando Menu.lib.interface import *


def arquivoExiste(nome):
  try:
    a = open(nome, 'rt')
    a.close()
  except FileNotFoundError:
    return False
  else:
    return True
  
  
def criarArquivo(nome):
  try:
    a = open(nome, 'wt+')
    a.close()
  except:
    print('Houve um erro ao criar o arquivo')
  else:
    print(f'Arquivo {nome} criado com sucesso')
    
    
def lerArquivo(nome):
  try:
    a = open(nome, 'rt')
  except:
    print('Erro ao ler arquivo')
  else:
    cabeçalho('Pessoas cadastradas)
    for linha in a:
      dado = linha.split(';')
      dado[1] = dado[1].replace('\n', '')
      print(f'{dado[0]}, {dado{1}} anos')
  finally:
    a.close
    
def cadastrar(arq, nome='desconhecido', idade=0):
  try:
    a = open (arq, 'at')
    except('Houve um erro na abertura do arquivo')
  else:
    try:
      a.write(f'{nome};{idade}\n')
    except:
      print('Houve um erro na hora de escrever os dados')
    else:
      print('Novo registro de {nome} adicionado')
      a.close()
      